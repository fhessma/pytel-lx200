# pytel_lx200/meade_api.py

"""
A dictionary containing the Meade LX200 command set.

The main keys are the command descriptions, the values are dictionaries containing
	'key'		the FITS key,
	'cmd'		the actual command to be sent via the serial port
	'input'		the input format for the command
	'output'	the output format of the response.

See https://www.meade.com/support/LX200CommandSet.pdf for more details.
"""

from enum import Enum

# FOR SetBaudeRate
BaudeRateCodes = { 57600:1, 38400:2, 28800:3, 19200:4, 14400:5, 9600:6, 4800:7, 2400:8, 1200:9 }


MEADE_API = { \
'HaltSlewing':		{'key':None,		'cmd':':Q#',	'input':None,							'output':None}, \
'HaltTelescope':	{'key':None,		'cmd':':T000.000#',	'input':None,						'output':'{0:1s}'}, \
'GetCurrentDate':	{'key':'TEL-DATE',	'cmd':':GC#',	'input':None,							'output':'{0:2s}/{1:2s}/{1:2s}#'}, \
'GetEastLongitude':	{'key':'TEL-LONG',	'cmd':':Gg#',	'input':None,							'output':'{0:1s}{1:3d}*{2:2d}#'}, \
'GetFirmwareDate':	{'key':None,		'cmd':':GVD#',	'input':None,							'output':'{0:s}#'}, \
'GetFirmwareNumber':{'key':None,		'cmd':':GVN#',	'input':None,							'output':'{0:s}#'}, \
'GetLocalTime':		{'key':'TEL-TIME',	'cmd':':GL#',	'input':None,							'output':'{0:2d}:{1:2d}:{2:2d}#'}, \
'GetMinElev':		{'key':'TEL-ZMAX',	'cmd':':Gh#',	'input':None,							'output':'{0:1s}{1:2d}#'}, \
'GetProductName':	{'key':'TEL-TYPE',	'cmd':':GVP#',	'input':None,							'output':'{0:s}#'}, \
'GetSiderialTime':	{'key':'SID-TIME',	'cmd':':GS#',	'input':None,							'output':'{0:2d}:{1:2d}:{2:2d}#'}, \
'GetTargetDec':		{'key':None,		'cmd':':Gd#',	'input':None,							'output':'{0:3d}*{1:2d}\'{2:2d}#'}, \
'GetTargetRA':		{'key':None,		'cmd':':Gr#',	'input':None,							'output':'{0:2d}:{1:2d}:{2:2d}#'}, \
'GetTelAlt':		{'key':'TEL-ALT',	'cmd':':GA#',	'input':None,							'output':'{0:1s}{1:2d}*{2:2d}\'{3:2d}#'}, \
'GetTelAz':			{'key':'TEL-AZ',	'cmd':':GZ#',	'input':None,							'output':'{0:3d}:*{1:2d}\'{2:2d}#'}, \
'GetTelDec':		{'key':'TEL-DEC',	'cmd':':GD#',	'input':None,							'output':'{0:1s}{1:2d}*{2:2d}\'{3:2d}#'}, \
'GetTelRA':			{'key':'TEL-RA',	'cmd':':GR#',	'input':None,							'output':'{0:2d}:{1:2d}:{2:2d}#'}, \
'GetTrackingRate':	{'key':None,		'cmd':':GT#',	'input':None,							'output':'{0:s}#'}, \
'GetUTCOffset':		{'key':'UTC-CORR',	'cmd':':GG#',	'input':None,							'output':'{0:1s}{1:2d}#'}, \
'HaltSlew':			{'key':None,        'cmd':':Q#',	'input':None,                           'output':None}, \
'SetAltAzMode':     {'key':None,        'cmd':':AA#',   'input':None,                           'output':None}, \
'SetBaudRate':		{'key':None,		'cmd':':B',		'input':'{0:1d}#',						'output':'{0:1d}#'}, \
'SetEastLongitude':	{'key':None,		'cmd':':Sg',	'input':'{0:3d}*{1:2d}#',				'output':'{0:1d}#'}, \
'SetLatitude':		{'key':None,		'cmd':':St',	'input':'{0:1s}{1:2d}*{2:2d}#',			'output':'{0:1d}#'}, \
'SetLocalTime':		{'key':None,		'cmd':':SL',	'input':'{0:2d}:{1:2d}:{2:2d}#', 		'output':'{0:1d}#'}, \
'SetLunarRate':		{'key':None,		'cmd':':TL#',	'input':None,				i			'output':None}, \
'SetMinElev':		{'key':None,		'cmd':':So',	'input':'{0:2d}*#',						'output':'{0:1d}#'}, \
'SetMinElev':		{'key':None,		'cmd':':Sh',	'input':'{0:2d}#',						'output':'{0:1d}#'}, \
'SetPolarMode':     {'key':None,        'cmd':':AP#',   'input':None,                           'output':None}, \
'SetSiderialTime':	{'key':None,		'cmd':':SS',	'input':'{0:2d}:{1:2d}:{2:2d}#',		'output':'{0:1d}#'}, \
'SetSiderealRate':	{'key':None,		'cmd':':TQ#',	'input':None,				i			'output':None}, \
'SetSlewCentering':	{'key':None,		'cmd':':RC#',	'input':None,							'output':None}, \
'SetSlewFind':		{'key':None,		'cmd':':RM#',	'input':None,							'output':None}, \
'SetSlewGuiding':	{'key':None,		'cmd':':RG#',	'input':None,							'output':None}, \
'SetSlewMax':		{'key':None,		'cmd':':RS#',	'input':None,							'output':None}, \
'SetTargetDec':		{'key':None,		'cmd':':Sd',	'input':'{0:1s}{1:2d}*{2:2d}:{3:2d}#',	'output':'{0:1d}#'}, \
'SetTargetRA':		{'key':None,		'cmd':':Sr',	'input':'{0:2d}:{1:2d}:{2:2d}#',		'output':'{0:1d}#'}, \
'SetUTCOffset':		{'key':None,		'cmd':':SG#',	'input':'{0:1s}{1:4.1f}#',				'output':'{0:1d}#'}, \
'SlewEast':			{'key':None,		'cmd':':Me#',	'input':None,							'output':None}, \
'SlewNorth':		{'key':None,		'cmd':':Mn#',	'input':None,							'output':None}, \
'SlewSouth':		{'key':None,		'cmd':':Ms#',	'input':None,							'output':None}, \
'SlewWest':			{'key':None,		'cmd':':Mw#',	'input':None,							'output':None}, \
'SlewToTarget':		{'key':None,		'cmd':':MS#',	'input':None,							'output':'{0:1d}{2:s}#'}, \
'TogglePrecision':	{'key':None,		'cmd':':P#',	'input':None,							'output':'{0:1s}{1:s}#'}, \
'ToggleTimeFormat':	{'key':None,		'cmd':':H',		'input':None,							'output':None} \
}
