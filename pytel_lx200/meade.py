# pytel_lx200/meade.py 

"""
Simple serial driver for a Meade telescope using the LX200 API.
"""

import serial
import threading

from astropy             import units as u
from astropy.coordinates import SkyCoord,Longitude
from astropy.time        import Time

from pytel.utils.threads import LockWithAbort

from api   import MEADE_API
from utils import min_str_length, scanf

log = logging.getLogger(__name__)


class LX200 (object) :
	def __init__ (self, config=None, *args, **kwargs) :
		self._serial = None
		self._command_lock = threading.Lock()
		self._lock_moving  = threading.Lock()
		self._abort_move   = threading.Event()
		self._config = config

    def command (self, cmd, indata=None) :
        with self._command_lock :
            res = self._cmd(cmd,indata)
        return res

	def _cmd (self, cmd, indata=None, parse=True) :
		"""
		Sends the given command to a Meade telescope via a serial connection using the
		MEADE_INTERFACE dictionary info key.
		FUTURE: If the output format is not None, then the output is placed in an array
		and returned.
		"""
		if self._serial == None :
			log.error ('No serial port connection : open() telescope!')
			return None

        # pass command through without parsing and indata
        if not parse :
            command = cmd
            
        # construct command using indata
        else :
    		if cmd not in MEADE_API :
    			log.error ('Unknown LX200 command : {0}'.format(cmd))
    			return None

	    	res = None
	    	info = MEADE_API[cmd]
	    	command = info['cmd']

	    	# fill command with input data and send
	    	if info['iformat'] is not None :
	    		command += info['iformat'].format(*indata)	# ISN'T PYTHON AMAZING!

	    self._serial.write (command)

		# read the return string
		if info['oformat'] is not None :
			l = min_str_length(info['oformat'])
			res = ''
			for i in range(l) :
				s = str(self._serial.read (1))
				if len(s) > 0 :
					res += s
				if s == '#' :
					break
			res = scanf (info['oformat'],res)

		return res		# NEED TO RE-FORMAT ACCORDING TO oformat AND OUTPUT AS ARRAY !!!

	def sidereal_time (self) :
		""" Returns S.T. as hours,minutes,seconds """
		t = Time.now()
		long = 0.
		if 'longitude' in self._config :
			long = longitude
		l = Longitude(long*u.deg)
		h,m,s = t.sidereal_time('mean',longitude=l).hms
		return h,m,s

	def open (self) -> bool:
		# init serial connection
		device = '/dev/ttyUSB0'
		baudrate = 9600
		bits     = 8
		parity   = serial.NO_PARITY
		stopbit  = 1
		mount    = 'polar'
		if self._config is not None :
			if 'device' in self._config :
				device = self._config['device']
			if 'baudrate' in self._config :
				baudrate = int(self._config['baudrate'])
			if 'bits' in self._config :
				bits = int(self._config['bits'])
			if 'parity' in self._config :
				p = self._config['parity'].lower()
				if p.startswith('n') :
				    p = serial.NO_PARITY
				elif p.startswith('e') :
				    p = serial.EVEN_PARITY
				elif p.statswith('o') :
				    p = serial.ODD_PARITY
			if 'stopbits' in self._config :
				stopbits = int(self._config['stopbits'])
            if 'mount' in self._config :
                mount = self._config['mount']

		log.info('Connecting to telescope via serial device {0:s} ...'.format(device))
		self._serial = serial.Serial(device,baudrate=baudrate,parity=parity)
		try :
			self._serial.open()
		except ValueError :
			log.error('ERROR: serial device configuration error')
			self._serial = None
			return False
		except SerialException as e :
			log.error('ERROR: could not connect via serial device {0:s}: {1}'.format(device,e))
			self._serial.close()
			self._serial = None
			return False

		# set position precision to "HIGH..."
		prec = self.command ('TogglePrecision')
		if prec.startswith ('L') :	# "LOW..."
			prec = self.command ('TogglePrecision')

		# set time
		fmt = self.command ('SetLocalTime',22,23,24)
		t = self.command ('GetLocalTime')
		if t[0] != 22 :
			fmt = self.command ('ToggleTimeFormat')
		t = Time.now()
		h,m,s = t.hms
		ret = self.command ('SetLocalTime',h,m,int(s))

		# set siderial time
		h,m,s = self.sidereal_time ()
		ret = self.command ('SetSiderealTime',h,m,int(s))

		# set mount
		if mount == 'altaz' :
		    ret = self.command ('SetTerrestrialMode')
		else :
    		ret = self.command ('SetPolarMode')

		# set tracking rate
		ret = self.command ('SetSiderealRate')
		return True

	def status (self, *args, **kwargs) -> dict:
		s = {}
		s['CurrentDate']    = self.command ('GetCurrentDate')
		s['EastLongitude']  = self.command ('GetEastLongitude')
		s['FirmwareDate']   = self.command ('GetFirmwareDate')
		s['FirmwareNumber'] = self.command ('GetFirmwareNumber')
		s['LocalTime']      = self.command ('GetLocalTime')
		s['MinElev']        = self.command ('GetMinElev')
		s['ProductName']    = self.command ('GetProductName')
		s['SiderealTime']   = self.command ('GetSiderealTime')
		s['TargetDec']      = self.command ('GetTargetDec')
		s['TargetRA']       = self.command ('GetTargetRA')
		s['TelAlt']         = self.command ('GetTelAlt')
		s['TelAz']          = self.command ('GetTelAz')
		s['TelRA']          = self.command ('GetTelRA')
		s['TelDec']         = self.command ('GetTelDec')
		s['TrackingRate']   = self.command ('GetTrackingRate')
		s['UTCOffset']      = self.command ('GetUTCOffset')
		return s

	def track (self, ra: float, dec: float) -> bool:
		"""
		Start going to ra,dec in degrees.
		"""
		# set target coordinates
		target = SkyCoord (ra,dec,unit=(u.deg,u.deg))
		h,m,s = target.ra.to('hourangle').hms
		ret = self.command ('SetTargetRA',h,m,int(s))
		d,m,s = target.dec.dms
		ret = self.command ('SetTargetDec',d,m,int(s))

		# slew to target
		ret = self.command ('SlewToTarget')
		return True

	def is_tracking (self, accur=10.) -> bool:
		"""
		Telescope is tracking if target position equals telescope position 
		to the given accuracy in arcseconds.
		"""
		target = SkyCoord (self.command ('GetTargetRA'),self.command ('GetTargetDec'),unit=(u.hourangle,u.deg))
		tel    = SkyCoord (self.command ('GetTelRA'),   self.command ('GetTelDec'),   unit=(u.hourangle,u.deg))
		return tel.separation (target) < 10.*u.arcsec

	def altaz (self) :
		alt = self.command ('GetTelAlt')
		az  = self.command ('GetTelAz')
		return alt,az

    def radec (self) :
        ra  = self.command ('GetTelRA')
        dec = self.command ('GetTelDec')
        return ra,dec
        
	def park (self) :
	    with LockWithAbort (self._lock_moving, self._abort_move) :
	        self._park (self._abort_move)

    def _park (self, abort_event: threading.Event) :
		# get Dec of zenith
		lat = 45.*u.deg
		if 'latitude' in self._config :
			lat = self._config['latitude']*u.deg
		if lat > 0.*u.deg :
			alt = 90.*u.deg-lat
		else :
			alt = 90.*u.deg+lat
		d,m,s = alt.dms
		ret = self.command ('SetTargetDec',d,m,s)

		# get RA of zenith
		h,m,s = self.sidereal_time ()
		ret = self.command ('SetTargetRA',h,m,s)

		# slew to park position
		ret = self.command ('SlewToTarget')

		while not self.tracking () :
		    if abort_event.is_set() :
		        self.command ('Stop')
		        return False
			time.sleep (2.)
		return True

	def close (self) :
		log.info('Closing serial connection to telescope ...')
		self._serial.close()


if __name__ == '__main__' :
    """
	fmt = 'X{0:1s}{1:3d}#'
	s = 'X+123#'
	print (fmt,s,scanf(fmt,s))
    """
    
	def help ()
		print ('Commands:')
		print ('\t[q]uit\t\tquit programme')
		print ('\tconf\t\tprint configuration')
		print ('\tconf key val\t\tadd conf[key] = val (strings must be quoted)')
		print ('\topen [d]efault\t\topen telescope using defaults')
		print ('\topen [c]onfigured\t\topen telescope using configuration')
		print ('\t?\t\tthis message')

	tel = LX200 ()
	quit = False
	conf = {}

	while not quit :
		ret = input('LX200> ').tolower().trim()
		if ret == 'quit' or ret='q' :
			tel.close()
			sys.exit(0)
		elif ret == 'conf' :
			print (conf)
		elif ret.startswith('conf') :
			things = ret.split()
			key = things[1]
			val = things[2]
			if val.startswith('\'') or val.startswith('"') :
				conf[key] = val[1:-1]
			if '.' in val :
				conf[key] = float(val)
			else :
				conf[key] = int(val)
		elif ret.startswith ('open d') :	# OPEN DEFAULT
			tel.open()
		elif ret.startswith ('open c') :	# OPEN CONFIGURED
			tel.open(conf=conf)
		elif ret.startswith ('! ') :
		    cmd = ret[2:]
		    res = tel.command(cmd,parse=False)
		    print (res)
		elif ret == '?' :
			help()
		else :
			stat = tel.status()
			print (stat)

