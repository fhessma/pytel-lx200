# pytel_lx200/telescope.py

"""
Based on pilartelescope.py
Version 0.1 (2018-JUN-27), F.V. Hessman
"""

import logging
import serial
import threading
import time

from astropy             import units as u
from astropy.coordinates import SkyCoord

from pytel.interfaces                      import IFitsHeaderProvider
from pytel.modules                         import timeout
from pytel.modules.telescope.basetelescope import BaseTelescope
from pytel.utils.threads                   import LockWithAbort

from meade import LX200

log = logging.getLogger(__name__)


class LX200Telescope(BaseTelescope):
	def __init__(self, *args, **kwargs):
		BaseTelescope.__init__(self, thread_funcs=[self._meade_update], *args, **kwargs)
        self._telescope_status = BaseTelescope.IDLE
		# LX200 driver
		self._driver = None

	@classmethod
	def default_config(cls):
		cfg = super().default_config()
		cfg['serial'] = {
			'device': '/dev/ttyUSB0',
			'baudrate': 9600,
			'bits': 8,
			'stopbits': 1,
			'parity':'N'
			}
		return cfg

	def open (self) -> bool:
		if not super().open(self):
			return False

		# init telescope object
		self._driver = LX200(config=cfg)
		if self._driver is None or not self._driver.open (config=cfg) :
			return False

		# success
		return True

    def close (self) -> bool:
        ok = True
        if self._driver is not None :
            ok = self._driver.close()
        return super().close() and ok

	@timeout(300000)
	def park (self, *args, **kwargs) -> bool:
		with LockWithAbort (self._lock_moving, self._abort_move) :
		    return self._park (self._abort_move)

    def _park (self, abort_event: threading.Event) -> bool:
		log.info('Parking telescope')
		self._telescope_status = BaseTelescope.Status.SLEWING
		self._driver.park()
		while not self._driver.is_parked () :
		    if abort_event.is_set() :
		        self._driver.halt_telescope ()
		        self._telescope_status = BaseTelescope.Status.IDLE
		        return False
		    time.sleep(2)
		self._telescope_status = BaseTelescope.Status.PARKED
        return True

    @timeout(300000)
    def track (self, ra: float, dec: float, *args, **kwargs) -> bool:
        with LockWithAbort (self._lock_moving, self._abort_move) :
            return self._track (ra,dec)

	def _track (self, ra: float, dec: float, abort_event: threading.Event) -> bool:
		# start tracking
		log.info('Slewing to RA=%.5f, Dec=%.5f', ra, dec)
		self.telescope_status = BaseTelescope.Status.SLEWING
        self._driver.track(ra,dec)
        while not self._driver.is_tracking() :
            if abort_event.is_set() :
                self._driver.halt_telescope ()
                self._telescope_status = BaseTelescope.Status.IDLE
                return False
            time.sleep(1)
        self._telescope_status = BaseTelescope.Status.TRACKING
        return True

	def status (self, *args, **kwargs) -> dict:
		# get parent
		s = super().status(*args, **kwargs)

        # get current status
        ra,dec = self._driver.radec ()
        alt,az = self._driver.altaz ()
        stats  = self._driver.status ()
		s['LX200'] = dict(stats)
        
        d = {}
        p = {}
        if 'ITelescope' in s :
            d = s['ITelescope']
            if 'Position' in d :
                p = d['Position']
            else :
                d['Position'] = p
        d['Status'] = self._telescope.status.name,
        p['RA']  = ra
        p['Dec'] = dec
        p['Alt'] = alt
        p['Az']  = az

        # finished  
		return s

	def get_fits_headers(self, *args, **kwargs) -> dict:
		# get current position via the status dictionary
		stats = self._driver.status()
		hdr = []

		ra,dec = self._driver.radec()
		alt,az = self._driver.altaz()

		hdr['CRVAL1'] = ra
		hdr['TEL-RA'] = ra
		hdr['RA'] = stats['TelRA']

		hdr['CRVAL2']   dec
		hdr['TEL-DEC'] = dec
		hdr['Dec'] = stats['TelDec']

		hdr['TEL-ALT'] = alt
		hdr['TEL-AZ']  = az
		return hdr

