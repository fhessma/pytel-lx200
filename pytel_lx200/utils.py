# pytel_lx200/utils.py

def min_str_length (fmt) :
	"""
	Returns the explicit minimum length of a string given by a given format.
	Note: {0}-like entries assumed to contribute 1!
	"""
	l = 0
	parts = fmt.split('{')				# e.g. 'a{}b{1}c{2:5.3f}d' -> ['a','}b','1}c','2:5.3f}d']
	for part in parts :
		stuff = part.split('}')			# e.g. '}b' -> ['','b'], '2:5.3f}d' -> ['2:5.3f','d']
		for s in stuff :
			if ':' in s :
				thing = s[s.index(':')]	# e.g. '5.3f'
				if '.' in thing and not thing.startswith('.') :
					l += int(thing[:thing.index('.')])
				else :
					l += int(thing[index('.'):-1])
			else :
				l += len(s)
	return l

def scanf (fmt,string) :
	"""
	Extract information from string using a python output format as an input format.
	"""
	istr = 0
	ifmt = 0
	l = len(string)
	result = []

	# skip text
	while istr < l :
		while string[istr] == fmt[ifmt] :
			istr += 1
			ifmt += 1
		if string[istr] != '{' :
			raise ValueError ('unable to parse {} using format {}!'.format(string,fmt))
			return None
		istr += 1
		try :
			idx = string[istr:].index('}')
		except ValueError as e :
			raise ValueError ('unable to parse {} using format {}!'.format(string,fmt))
			return None
		desc = string[istr:istr+idx]
		result.append(desc)
		print (desc)
		istr += idx

