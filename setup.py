from distutils.core import setup

setup(
    name='pytel-lx200',
    version='0.1',
    packages=['pytel_lx200'],
    url='',
    license='',
    author='Frederic V. Hessman',
    author_email='hessman@astro.physik.uni-goettingen.de',
    description='pytel module for telescopes using the Meade LX200 API',
    requires=[]
)
